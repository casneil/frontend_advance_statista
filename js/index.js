const searchRoot = document.querySelector("[data-vue-search]");

/* This is an example Vue app, feel free to use your own code. By default, Vue 2.6 and Axios are available. */

Vue.component("SearchBar", {
  data() {
    return {
      searchTerm: "statista",
    };
  },
  template: `
        <div class="pos-relative searchApp__container" aria-label="Search bar">
             <input v-model="searchTerm" placeholder="Search for statistics" type="text" />
             <button type="submit" class="button button--primary searchApp__submitButton" >
                Search
             </button>
        </div>
    `,
});

Vue.component("SearchResults", {
  data() {
    return {
      searchResults: [],
    };
  },
  methods: {
    getResults: async function () {
      /* Built URL using javascript's URL API. Although this is not needed currently, it could be usefull for later use.
         For more advance queries with query params use the searchParams.append method e.g ->  endPoint.searchParams.append("term", "statista");
         Reference = https://developer.mozilla.org/en-US/docs/Web/API/URL/URL
       */
      endPoint = new URL("https://cdn.statcdn.com/static/application/search_results.json");
      try {
        const request = await fetch(endPoint.toString());
        const response = await request.json();
        this.searchResults = response.items;
      } catch (error) {
        // Simple error handling
        window.alert("Something went wrong. Please try again later");
      }
    },
  },
  mounted() {
    this.getResults();
  },
  template: `
        <section class="searchApp__results">
          <div class="panelCard padding-all-20">
            <h2 class="sectionRooftitle">Showing {{searchResults.length}} Results</h2>
          </div>
          <div class="panelCard panelCard--hover  padding-all-20 margin-top-15 " v-for="result in searchResults":key="result.identifier">
            <div class="flex">
              <img :src="result.teaser_image_urls[2].src" :alt="result.title"/>
              <h5 class="margin-left-10 text-color--primary font-size-sm">{{dayjs(result.date).format("MMM D, YYYY")}}</h5>
              <div class="margin-left-5 margin-right-5">|</div>
              <h5 class="font-size-l">{{result.title.split(" ")[0].replace(":", "")}}</h5>
            </div>
            <h4 class="font-size-l margin-top-5 margin-bottom-5">{{result.title}}</h4>
            <p>{{result.description}}</p>
          </div>
        </section>
    `,
});

Vue.component("SearchApp", {
  template: `
        <div>
          <SearchBar />
          <SearchResults/>
        </div>
        `,
});

if (searchRoot) {
  new Vue({
    el: searchRoot,
    render(createElement) {
      return createElement("SearchApp");
    },
  });
}
