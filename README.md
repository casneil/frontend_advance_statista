# Frontend Exercise Statista

The aim of this exercise was to:

1. Create a user interface for displaying search results.
2. The results should at least include title and description.
3. Show the number of results for the given keyword (in this case “Statista”).
4. Keep usability and accessibility in mind. Code for real world conditions.

---

### Features

- View results data for search term.
- Title, description, date, image infomation rendered on page.
- Total number of results for given search term.

---

#### Screenshots

![picture](screenshots/app.png)
![picture](screenshots/lighthouse.png)

[View React version](https://bitbucket.org/casneil/frontend_advance_statista_react/src/master/)
